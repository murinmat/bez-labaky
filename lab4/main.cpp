// Matej Murin <username:murinmat>

#include <string>
#include <cstdio>
#include <openssl/evp.h>
#include <openssl/pem.h>

#define BUFFER_LENGTH 1024
#define BLOCK_SIZE 128

// ============================================================================
class FileNotFoundException : public std::exception
{
public:
    explicit FileNotFoundException(std::string  fileName);

    const char* what() const noexcept override;
private:
    std::string m_FileName;
};

FileNotFoundException::FileNotFoundException(std::string fileName)
: m_FileName(std::move(fileName))
{
}

const char * FileNotFoundException::what() const noexcept
{
    return m_FileName.c_str();
}

// ============================================================================
class MessageCipher
{
public:
    explicit MessageCipher(const std::string& inputFile, const std::string& outputFile);

    ~MessageCipher();

    int encrypt(const std::string& pubKey);

    int decrypt(const std::string& privateKey);

private:
    static void writeHeader(FILE* fileOut,
                            const std::string& opMode,
                            int bits,
                            int keyLength,
                            const unsigned char* iv,
                            const unsigned char* ek,
                            int ekSize);

    FILE*               m_InputFile;
    FILE*               m_OutputFile;
    EVP_CIPHER_CTX*     m_CipherContext;
    unsigned char       m_Buffer[BUFFER_LENGTH];
    unsigned char       m_CryptBuffer[BUFFER_LENGTH];
};

MessageCipher::MessageCipher(const std::string& inputFile, const std::string& outputFile)
{
    OpenSSL_add_all_ciphers();
    m_InputFile = fopen(inputFile.c_str(), "rb");
    if (!m_InputFile)
        throw FileNotFoundException(inputFile);

    m_OutputFile = fopen(outputFile.c_str(), "wb");
    if (!m_OutputFile)
        throw FileNotFoundException(outputFile);

    m_CipherContext = EVP_CIPHER_CTX_new();
}

MessageCipher::~MessageCipher()
{
    fclose(m_InputFile);
    fclose(m_OutputFile);
    EVP_CIPHER_CTX_free(m_CipherContext);
}

void MessageCipher::writeHeader(FILE* fileOut,
                                const std::string &opMode,
                                int bits,
                                int keyLength,
                                const unsigned char *iv,
                                const unsigned char *ek,
                                int ekSize)
{
    fwrite(opMode.c_str(), sizeof(char),  opMode.length(), fileOut);
    fwrite(&bits, sizeof(int), 1, fileOut);
    fwrite(&keyLength, sizeof(int), 1, fileOut);
    fwrite(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, fileOut);
    fwrite(ek, sizeof(unsigned char), ekSize, fileOut);
}

int MessageCipher::encrypt(const std::string &pubKey) {
    FILE *tmp = fopen(pubKey.c_str(), "rb");
    if (!tmp)
        throw FileNotFoundException(pubKey);
    EVP_PKEY *key = PEM_read_PUBKEY(tmp, nullptr, nullptr, nullptr);
    fclose(tmp);

    // cipher type
    const EVP_CIPHER *cipher = EVP_aes_128_cbc();
    unsigned char iv[EVP_MAX_IV_LENGTH];
    auto ek = new unsigned char[EVP_PKEY_size(key)];

    int cipherKeyLength;
    if (!EVP_SealInit(m_CipherContext, cipher, &ek, &cipherKeyLength, iv, &key, 1))
    {
        printf("Unable to initialize seal!\n");
        return 1;
    }

    // write a header to the file
    writeHeader(m_OutputFile, "cbc", 128, cipherKeyLength, iv, ek, EVP_PKEY_size(key));

    // Encrypt data
    int bufferLength, cryptLength;
    while ((bufferLength = fread(m_Buffer, sizeof(unsigned char), BLOCK_SIZE, m_InputFile)))
    {
        EVP_SealUpdate(m_CipherContext, m_CryptBuffer, &cryptLength, m_Buffer, bufferLength);
        fwrite(m_CryptBuffer, sizeof(unsigned char), cryptLength, m_OutputFile);
    }
    if (!EVP_SealFinal(m_CipherContext, m_CryptBuffer, &cryptLength))
    {
        printf("Unable to finalize seal!\n");
        return 1;
    }

    fwrite(m_CryptBuffer, sizeof(unsigned char), cryptLength, m_OutputFile);

    delete[] ek;
    EVP_PKEY_free(key);

    printf("Encryption successful.!\n");

    return 0;
}

int MessageCipher::decrypt(const std::string& privateKey) {
    FILE *tmp = fopen(privateKey.c_str(), "rb");
    if (!tmp)
        throw FileNotFoundException(privateKey);
    EVP_PKEY *key = PEM_read_PrivateKey(tmp, nullptr, nullptr, nullptr);
    fclose(tmp);

    // header of file
    char opMode[4];
    fread(&opMode, sizeof(char), 3, m_InputFile);
    opMode[3] = '\0';
    int bits;
    fread(&bits, sizeof(int), 1, m_InputFile);
    int keyLength;
    fread(&keyLength, sizeof(int), 1, m_InputFile);
    unsigned char iv[EVP_MAX_IV_LENGTH];
    fread(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, m_InputFile);
    auto *ek = new unsigned char[keyLength];
    fread(ek, sizeof(unsigned char), keyLength, m_InputFile);

    // get cipher type
    const EVP_CIPHER *cipher;
    if (std::string(opMode) == "cbc")
    {
        if (bits == 128) cipher = EVP_aes_128_cbc();
        else if (bits == 256) cipher = EVP_aes_256_cbc();
        else return 1;
    }
    else if (std::string(opMode) == "ecb")
    {
        if (bits == 128) cipher = EVP_aes_128_ecb();
        else if (bits == 256) cipher = EVP_aes_256_ecb();
        else return 1;
    }

    // Decrypt data
    if (!EVP_OpenInit(m_CipherContext, cipher, ek, keyLength, iv, key))
    {
        printf("Unable to initialize seal!\n");
        return 1;
    }

    int bufferLength, cryptLength;
    while ((bufferLength = fread(m_Buffer, sizeof(unsigned char), BLOCK_SIZE, m_InputFile)))
    {
        EVP_OpenUpdate(m_CipherContext, m_CryptBuffer, &cryptLength, m_Buffer, bufferLength);
        fwrite(m_CryptBuffer, sizeof(unsigned char), cryptLength, m_OutputFile);
    }

    if (!EVP_OpenFinal(m_CipherContext, m_CryptBuffer, &cryptLength))
    {
        printf("Unable to finalize seal!\n");
        return 1;
    }

    fwrite(m_CryptBuffer, sizeof(unsigned char), cryptLength, m_OutputFile);

    delete[] ek;
    printf("Decryption successful.!\n");

    return 0;
}

int main(int argc, char* argv[])
{
    // define usage
    std::string usage = "Usage: ";
    usage += argv[0];
    usage += " -e/-d inputFile outputFile yourKey.pem";

    // check user input
    if (argc != 5)
    {
        printf("%s\n", usage.c_str());
        return 1;
    }

    MessageCipher m(argv[2], argv[3]);

    // check if we are encrypting or decrypting
    if (std::string(argv[1]) == "-e")
    {
        return m.encrypt(argv[4]);
    }
    // decrypt
    else if (std::string(argv[1]) == "-d")
    {
        return m.decrypt(argv[4]);
    }
    else
    {
        printf("Unknown argument: %s\n", argv[1]);
        printf("%s\n", usage.c_str());
        return 1;
    }
}

