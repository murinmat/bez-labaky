#!/bin/bash


if cmp $1 $2; then
    echo "Files match! :)"
else
    echo "Files do not match :("
fi
