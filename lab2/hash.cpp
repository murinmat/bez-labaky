#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>
#include <string>
#include <cmath>
#include <vector>
#include <openssl/evp.h>

// ============================================================================
struct Collision
{
    std::string m_OT1,
                m_ST1,
                m_OT2,
                m_ST2;
};
// ============================================================================
class CollisionFinder
{
public:
    // ------------------------------------------------------------------------
    CollisionFinder();
    // ------------------------------------------------------------------------
    ~CollisionFinder();
    /**
     * Find a collision of hashes with given minimal length
     * @param length is the first N hex chars that need to match
     */
    void findCollision                                  (int length);
    /**
     * Print a collision found by findCollion, if existent
     */
    void printCollision                                 () const;
private:
    /**
     * Get a cipher string from a given open text
     * @param ot is the given open text
     * @return cipher text of given open text
     */
    static std::string getCipher                        (const std::string& ot);
    /**
     * Get a random string with given minimal length
     * @param minLength is the minimal length
     * @return random string of minimal length
     */
    static std::string getRandomString                  (int minLength);
    /**
     * convert an unsigned char to a hex representation of string
     * @param x is the given char
     * @return string representation of the given char in hex
     */
    static std::string hexToStr                         (const unsigned char x);

    std::vector<char>                  m_Letters;
    Collision*                         m_FoundCollision;
};
// ----------------------------------------------------------------------------
CollisionFinder::CollisionFinder()
: m_FoundCollision(nullptr)
{
    srand(time(NULL));
    OpenSSL_add_all_ciphers();
    OpenSSL_add_all_digests();
}
// ----------------------------------------------------------------------------
CollisionFinder::~CollisionFinder()
{
    delete m_FoundCollision;
}
// ----------------------------------------------------------------------------
void CollisionFinder::findCollision(int length) {
    if (m_FoundCollision)
        delete m_FoundCollision;
    std::map<std::string, std::string> ciphers;

    double neededTries = pow(16, length) + 1;
    printf("Tries needed to find a collision of length %d is %f\n", length, neededTries);
    // we are to find a collision of first N hex symbols in this range, due to pigeonhole principle
    for(double i = 0; i < neededTries; ++i)
    {
        std::string ot = getRandomString(length + 1);
        // make sure the generated string differs from the already generated ones
        while (ciphers.find(ot) != ciphers.end())
            ot = getRandomString(length + 1);

        std::string st = getCipher(ot);
        // check if we already have a collision
        for(auto& t : ciphers) {
            // collision on first tho hex characters found here
            if (t.first.substr(0, length) == st.substr(0, length))
            {
                m_FoundCollision = new Collision();
                m_FoundCollision->m_ST1 = t.first;
                m_FoundCollision->m_OT1 = t.second;
                m_FoundCollision->m_ST2 = st;
                m_FoundCollision->m_OT2 = ot;
                return;
            }
        }
        ciphers[st]= ot;
    }
}
// ----------------------------------------------------------------------------
void CollisionFinder::printCollision() const
{
    if (!m_FoundCollision)
    {
        printf("No collision found yet!\n");
        return;
    }
    printf("Collision found:\n");
    printf("OT1: %s\n", m_FoundCollision->m_OT1.c_str());
    printf("ST1: %s\n", m_FoundCollision->m_ST1.c_str());
    printf("OT2: %s\n", m_FoundCollision->m_OT2.c_str());
    printf("ST2: %s\n", m_FoundCollision->m_ST2.c_str());
}
// ----------------------------------------------------------------------------
std::string CollisionFinder::getCipher(const std::string& ot)
{
    int i, res;
    char hashFunction[] = "sha256";  // zvolena hashovaci funkce ("sha1", "md5" ...)

    EVP_MD_CTX *ctx;  // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)
    int length;  // vysledna delka hashe

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if(!type) {
        printf("Hash %s neexistuje.\n", hashFunction);
        exit(1);
    }

    ctx = EVP_MD_CTX_create(); // create context for hashing
    if(ctx == NULL) exit(2);

    /* Hash the text */
    res = EVP_DigestInit_ex(ctx, type, NULL); // context setup for our hash type
    if(res != 1) exit(3);
    res = EVP_DigestUpdate(ctx, ot.c_str(), ot.length()); // feed the message in
    if(res != 1) exit(4);
    res = EVP_DigestFinal_ex(ctx, hash, (unsigned int *) &length); // get the hash
    if(res != 1) exit(5);

    EVP_MD_CTX_destroy(ctx); // destroy the context

    std::string result;
    for(i = 0; i < length; i++)
        result += hexToStr(hash[i]);

    return result;
}
// ----------------------------------------------------------------------------
std::string CollisionFinder::getRandomString(int minLength)
{
    int strSize = (rand() % 10) + minLength;
    std::string result = "";
    char start = 'a';

    for(int i = 0; i < strSize; ++i)
        result += start + rand() % 26;

    return result;
}
// ----------------------------------------------------------------------------
std::string CollisionFinder::hexToStr(const unsigned char x)
{
    std::string res;
    char c1 = x >> 4;
    if (c1 <= 9)
        c1 += '0';
    else
        c1 += 'a'-10;
    char c2 = x & 0x0f;
    if (c2 <= 9)
        c2 += 'a';
    else
        c2 += 'a'-10;

    res += c1;
    res += c2;
    return res;
}
// ----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("Usage: %s <collision length number>\n", argv[1]);
        return 1;
    }

    CollisionFinder c;
    c.findCollision(atoi(argv[1]));
    c.printCollision();

    return 0;
}

