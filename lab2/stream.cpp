#include <iostream>
#include <cstdlib>
#include <openssl/evp.h>
#include <cstring>
#include <string>
#include <vector>

// ============================================================================
class RC4
{
public:
    RC4();
    /**
     * cipher given open text using the given secret key
     * @param ot is the open text
     * @param key is the secret key
     * @return the cipher of the open text
     */
    unsigned char* cipherText                               (unsigned char* ot, unsigned char* key) const;
    /**
     * Decipher an unknown message that was ciphered using the same secret key
     * @param knownOpenText is the known open text
     * @param knownTextCipher is the cipher of the known open text
     * @param unknownTextCipher is the cipher of the unknown open text
     * @return the unknown open text
     */
    static std::string decipherMessage                      (const unsigned char* knownOpenText,
                                                             const unsigned char* knownTextCipher,
                                                             const unsigned char* unknownTextCipher);
    /**
     * Convert bytes to their string representation
     * @param x is the pointer to byte array
     * @param len is the length of the array
     * @return byte array string representation
     */
    static std::string bytesToHexStr                        (const unsigned char* x, int len);
    /**
     * Convert hex vector to string using hex to char conversion
     * @param x is the hex values vector
     * @return string representation of the x vector
     */
    static std::string hexToStr                             (const std::vector<uint8_t>& x);
private:
    /**
     * Convert an int value to char
     * @param v is the int
     * @return char representation of the int
     */
    static unsigned char valToChar                          (int v);
    /**
     * Convert a char value to int
     * @param c is the char
     * @return int representation of the char
     */
    static unsigned int charToValue                         (unsigned char c);
    /**
     * Convert bytes array to chars array
     * @param x is the bytes array
     * @return char array of the bytes
     */
    static std::vector<unsigned char> bytesToChars      (const std::vector<uint8_t>& x);
    /**
     * Convert hex string to bytes
     * @param x is the hex string
     * @return vector of bytes from the hex string
     */
    static std::vector<uint8_t> hexStringToBytes            (const unsigned char* x);
    /**
     * XOR two vector's of bytes
     * @param x is first vector
     * @param y is second vector
     * @return vector of xor'd values of x and y
     */
    static std::vector<uint8_t> xorVectors                  ( const std::vector<uint8_t>& x,
                                                              const std::vector<uint8_t>& y);

    static unsigned char* strToHex                          (const unsigned char* x, int len);

    const EVP_CIPHER*                                       m_Cipher;
};
// ----------------------------------------------------------------------------
RC4::RC4()
{
    OpenSSL_add_all_ciphers();
    m_Cipher = EVP_get_cipherbyname("RC4");
}
// ----------------------------------------------------------------------------
unsigned char* RC4::cipherText(unsigned char *ot, unsigned char *key) const
{
    int res;
    unsigned char* st = new unsigned char[1024];  // sifrovany text
    unsigned char iv[EVP_MAX_IV_LENGTH] = "tralala";  // inicializacni vektor, doesn't matter because rc4

    int otLength = strlen((const char*) ot);
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) exit(2);

    /* Sifrovani */
    res = EVP_EncryptInit_ex(ctx, m_Cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx,  st, &tmpLength, ot, otLength);  // encryption of pt
    if(res != 1) exit(4);
    stLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, st + stLength, &tmpLength);  // get the remaining ct
    if(res != 1) exit(5);
    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return st;
}
// ----------------------------------------------------------------------------
std::string RC4::decipherMessage(   const unsigned char* knownOpenText,
                                    const unsigned char* knownTextCipher,
                                    const unsigned char* unknownTextCipher)
{
    unsigned char* tmp = strToHex(knownOpenText, 30);
    auto between = xorVectors(hexStringToBytes(knownTextCipher), hexStringToBytes(unknownTextCipher));
    auto result = xorVectors(between, hexStringToBytes(tmp));

    return hexToStr(bytesToChars(result));
}
// ----------------------------------------------------------------------------
unsigned char RC4::valToChar(int v)
{
    if (v <= 9)
        return v+'0';
    return v+'a'-10;
}
// ----------------------------------------------------------------------------
unsigned int RC4::charToValue(unsigned char c)
{
    if (c <= '9')
        return c-'0';
    return c-'a'+10;
}
// ----------------------------------------------------------------------------
unsigned char* RC4::strToHex(const unsigned char *x, int len)
{
    unsigned char* result = new unsigned char[(len*2)+1];
    int i = 0;
    int loop = 0;
    while (i < len) {
        result[loop++] = RC4::valToChar((int) x[i] / 16);
        result[loop++] = RC4::valToChar((int) x[i] % 16);
        i += 1;
    }

    result[(len*2)+1] = '\0';
    return result;
}
// ----------------------------------------------------------------------------
std::vector<uint8_t> RC4::xorVectors(   const std::vector<uint8_t>& x,
                                        const std::vector<uint8_t> &y)
{
    std::vector<uint8_t> tmp;
    for(unsigned long i = 0; i < x.size(); ++i)
        tmp.emplace_back(x[i]^y[i]);
    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<uint8_t> RC4::hexStringToBytes(const unsigned char *x)
{
    std::vector<uint8_t> tmp;
    int i = 0;
    while (x[i] != '\0')
    {
        tmp.emplace_back(RC4::charToValue(x[i]));
        i += 1;
    }

    return tmp;
}
// ----------------------------------------------------------------------------
std::vector<unsigned char> RC4::bytesToChars(const std::vector<uint8_t> &x)
{
    std::vector<unsigned char> tmp;
    for(unsigned long i = 0; i < x.size(); i += 1)
        tmp.emplace_back(RC4::valToChar((int)x[i]));
    return tmp;
}
// ----------------------------------------------------------------------------
std::string RC4::bytesToHexStr(const unsigned char *x, int len)
{
    std::string res;
    int i = 0;
    while(i < len)
    {
        res += RC4::valToChar(x[i]>>4);
        res += RC4::valToChar(x[i] & 15);
        i += 1;
    }
    return res;
}
// ----------------------------------------------------------------------------
std::string RC4::hexToStr(const std::vector<uint8_t> &x)
{
    unsigned long i = 0;
    std::string result;
    while(i != x.size())
    {
        unsigned left = RC4::charToValue(x[i]);
        unsigned right = RC4::charToValue(x[i+1]);
        result += (char)((left*16)+right);
        i += 2;
    }

    return result;
}
// ----------------------------------------------------------------------------
void outputMyCiphers()
{
    RC4 rc4;
    unsigned char my_key[17] = "my secret key :)";
    unsigned char my_message[31] = "I love BEZ very much haha xoxo";
    unsigned char known_text[31] = "abcdefghijklmnopqrstuvwxyz0123";

    auto c1 = rc4.cipherText(my_message, my_key);
    auto c2 = rc4.cipherText(known_text, my_key);

    printf("My message 1: %s\n", rc4.bytesToHexStr(c1, 30).c_str());
    printf("My message 2: %s\n", rc4.bytesToHexStr(c2, 30).c_str());
}
// ----------------------------------------------------------------------------
void decipherMessages(const unsigned char* st1, const unsigned char* st2)
{
    RC4 rc4;
    unsigned char known_text[31] = "abcdefghijklmnopqrstuvwxyz0123";
    auto deciphered = rc4.decipherMessage(known_text, st1, st2);

    printf("Your secret message: %s\n", deciphered.c_str());
}
// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cout << "Error parsing input!" << std::endl;
        std::cout << "Usage: " << argv[0] << " cipher1 cipher2" << std::endl;
        exit(1);
    }

    // create and output two RC4 ciphers on texts using the same key
    outputMyCiphers();

    // decipher argv[2] to it's open text form and output it
    decipherMessages((unsigned char*)argv[1], (unsigned char*)argv[2]);

    return 0;
}
