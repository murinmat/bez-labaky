// Matej Murin <username:murinmat>

#include <iostream>
#include <string>
#include <fstream>
#include <openssl/evp.h>

class BMPCipherer {
public:
    BMPCipherer();
    bool                    cipherFile( const std::string&              inputFile,
                                        const std::string&              cipherName,
                                        const unsigned char             secretKey[EVP_MAX_KEY_LENGTH],
                                        const unsigned char             initVector[EVP_MAX_IV_LENGTH]);
private:
    bool                    loadFile(   const std::string&              inputFile,
                                        const std::string&              outputFile);
    bool                    cipherBy(   const std::string&              cipherName,
                                        const unsigned char             secretKey[EVP_MAX_KEY_LENGTH],
                                        const unsigned char             initVector[EVP_MAX_IV_LENGTH]);

    char                    m_Header[2];
    uint32_t                m_FileSize;
    uint32_t                m_DataStartPosition;
    std::ifstream           m_InputFile;
    std::ofstream           m_OutputFile;
};



BMPCipherer::BMPCipherer()
: m_Header(),
  m_FileSize(-1),
  m_DataStartPosition(-1)
{}

bool BMPCipherer::cipherFile(const std::string &inputFile,
                             const std::string &cipherName,
                             const unsigned char secretKey[EVP_MAX_KEY_LENGTH],
                             const unsigned char initVector[EVP_MAX_IV_LENGTH]) {
    std::string cipherType = cipherName.substr(cipherName.length()-3);
    return loadFile(inputFile, inputFile + "_" + cipherType + ".bmp") && cipherBy(cipherName, secretKey, initVector);
}

bool BMPCipherer::loadFile(const std::string& inputFile, const std::string& outputFile) {
    m_InputFile = std::ifstream(inputFile, std::ios::out | std::ios::binary );
    // check if file was able to be opened
    if (!m_InputFile) return false;

    char dummy;
    m_OutputFile = std::ofstream(outputFile, std::ofstream::binary);
    if (!m_OutputFile) return false;

    m_InputFile.read((char*)m_Header, 2);
    m_OutputFile.write((char*)m_Header, 2);

    m_InputFile.read((char*)&m_FileSize, 4);
    m_OutputFile.write((char*)&m_FileSize, 4);

    for(int i = 0; i < 4; ++i) {
        m_InputFile.read(&dummy, 1);
        m_OutputFile.write(&dummy, 1);
    }

    m_InputFile.read((char*)&m_DataStartPosition, 4);
    m_OutputFile.write((char*)&m_DataStartPosition, 4);

    // color palette
    char* pallette = new char[m_DataStartPosition - 14];
    m_InputFile.read(pallette, m_DataStartPosition - 14);
    m_OutputFile.write(pallette, m_DataStartPosition - 14);

    return true;
}

bool BMPCipherer::cipherBy(const std::string &cipherName,
                           const unsigned char secretKey[EVP_MAX_KEY_LENGTH],
                           const unsigned char initVector[EVP_MAX_IV_LENGTH]) {
    int res;
    const EVP_CIPHER * cipher;

    OpenSSL_add_all_ciphers();
    /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
    cipher = EVP_get_cipherbyname(cipherName.c_str());
    if(!cipher)
        return false;

    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == nullptr)
        return false;

    // context init - set cipher, key, init vector
    if(EVP_EncryptInit_ex(ctx, cipher, NULL, secretKey, initVector) != 1)
        return false;

    unsigned currentPos = m_DataStartPosition;
    char newData[8];
    unsigned char output[8];
    // cypher 8 bytes constantly
    while (currentPos < m_FileSize) {
        // read the new "OT"
        m_InputFile.read(newData, 8);
        // cipher it
        res = EVP_EncryptUpdate (ctx, output, &stLength, (unsigned char*)newData, 8);
        if (res != 1)
            return false;
        if (stLength != 8)
            std::cout << "St len was not 8 at: " << currentPos << std::endl;
        m_OutputFile.write((char*)&output, stLength);
        currentPos += 8;
    }

    res = EVP_EncryptFinal_ex(ctx, output, &tmpLength);  // get the remaining ct
    if(res != 1) return false;
    stLength += tmpLength;
    m_OutputFile.write((char*)&output, tmpLength);
    m_OutputFile.close();

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return true;
}


int main(int argc, char* argv[])
{
    if (argc != 2) {
        std::cout << "Usage: <input file>" << std::endl;
        return 1;
    }

    std::string inputFileName = argv[1];
    unsigned char secretKey[EVP_MAX_KEY_LENGTH] = "LelMatej";
    unsigned char initVector[EVP_MAX_IV_LENGTH] = "hahahahhahaaha";

    BMPCipherer myCipherer;
    // cipher using ecb
    if (!myCipherer.cipherFile(inputFileName, "des-ede3-ecb", secretKey, initVector) )
        std::cout << "ECB ciphering went wrong..." << std::endl;

    std::cout << "All good!" << std::endl;
    return 0;
}

